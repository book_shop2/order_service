
CREATE TABLE IF NOT EXISTS "order" (
    id UUID PRIMARY KEY,
    book_id UUID,
    description VARCHAR,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);
