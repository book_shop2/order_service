package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"order_service/config"
	"order_service/genproto/order_service"
	"order_service/grpc/client"
	"order_service/grpc/service"
	"order_service/pkg/logger"
	"order_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	order_service.RegisterOrderServiceServer(grpcServer, service.NewOrderService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
