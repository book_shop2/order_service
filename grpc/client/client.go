package client

import (
	"order_service/config"

	"order_service/genproto/catalog_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	BookCategoryService() catalog_service.BookCategoryServiceClient
}

type grpcClients struct {
	bookCategoryService catalog_service.BookCategoryServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// Catalog Service...
	connCatalogService, err := grpc.Dial(
		cfg.CatalogServiceHost+cfg.CatalogGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		bookCategoryService: catalog_service.NewBookCategoryServiceClient(connCatalogService),
	}, nil
}

func (g *grpcClients) BookCategoryService() catalog_service.BookCategoryServiceClient {
	return g.bookCategoryService
}
